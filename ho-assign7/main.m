// ******************************************************************************
//
//  Main.m
//  ho-assign7
//
//  Created by Henrique de Oliveira Carvalho on 2014-10-21.
//  Copyright (c) 2014 beta. All rights reserved.
//
// Problem Statement:
// • Create a class Student that has three NSMutableArrays of grades.
// • The Student constructor must verify that both first and last names
//   are between 2 and 20 characters long.
// • Create getters for both first and last names.
// • Create a class called StringFileUtils that is used to read from a file,
//   extract data of student.
// • Each student names and grades are available in their own files. Valid
//   grades are only case insensitive course codes IMG, CPS, and ENG and
//   grades from 0 to 100.
// • Any corrupted data should be ignored.
// • Read the files and fill the appropriate NSMutableArrays of grades for
//   each student.
// • The class Student should have three methods : getBestGrade,
//   getWorstGrade, and getAverageGrade that accepts a course code argument.
// • The class Student should have a method displayGrades of a specific grade
// • The class Student should have a method description that displays all
//   information about a Student.
// • Your driver main.m should contain a loop with a menu options asking the
//   user what actions he/she would like to do
//
// Inputs:   none
// Outputs:  Information about a student
//
// ******************************************************************************



#import <Foundation/Foundation.h>
#import "StringFileUtils.h"
#import "Student.h"

#define NSLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ## __VA_ARGS__] UTF8String]);

int main(int argc, const char * argv[])
{
  @autoreleasepool {

    NSString * line, * sFileOption, * sGradeOption;
    NSString * word, * key, * value;

    StringFileUtils * console = [[StringFileUtils alloc] init];

    NSLog(@"Enter the student number:");
    NSLog(@"1 _ Joseph Black");
    NSLog(@"2 _ James Bond");
    NSLog(@"3 _ Alice Wunderland");

    int iChoice = 0;

    while (iChoice != 1 && iChoice != 2 && iChoice != 3)
    {
      NSString * sChoice = [console getConsoleLine];
      iChoice = [sChoice intValue];

      if (iChoice == 1)
      {
        sFileOption = @"student1.txt";
      }
      else if (iChoice == 2)
      {
        sFileOption = @"student2.txt";
      }
      else if (iChoice == 3)
      {
        sFileOption = @"student3.txt";
      }
      else if (iChoice > 3 || iChoice < 1)
      {
        NSLog(@"Invalid number try again!");
      }
    }

    // read the file
    NSArray * dataLines;
    dataLines = [ console getNSArrayOfLinesFromFolder:@"problem7" andFilename:sFileOption ];

    Student * student = [[Student alloc] initMyArray];

    for (int pos = 0; pos < [dataLines count] - 1; pos++)
    {
      line = dataLines[pos];
      line = [line lowercaseString];

      NSScanner * scanner = [NSScanner scannerWithString:line];

      NSCharacterSet * skipCharSet = [NSCharacterSet characterSetWithCharactersInString:@": "];

      [scanner setCharactersToBeSkipped:skipCharSet];

      [scanner scanUpToCharactersFromSet:skipCharSet intoString:&word];
      key = word;

      [scanner scanUpToCharactersFromSet:skipCharSet intoString:&word];
      value =  word;

      // Adding the informations
      [student setKey:key andValue:value];

    }

    iChoice = 0;

    NSLog(@"What course grade do you want to see ?");
    NSLog(@"1 _ CPS");
    NSLog(@"2 _ ENG");
    NSLog(@"3 _ IMG");

    while (iChoice != 1 && iChoice != 2 && iChoice != 3)
    {
      NSString * sChoice = [console getConsoleLine];
      iChoice = [sChoice intValue];

      if (iChoice == 1)
      {
        sGradeOption = @"cps";
      }
      else if (iChoice == 2)
      {
        sGradeOption = @"eng";
      }
      else if (iChoice == 3)
      {
        sGradeOption = @"img";
      }
      else if (iChoice > 3 || iChoice < 1)
      {
        NSLog(@"Invalid number try again!");
      }
    }

    [student showCourseGrades:sGradeOption];

    NSLog(@" Best Grade of %@: %f", sGradeOption, [student getBestGrade:sGradeOption]);
    NSLog(@" Worst Grade of %@: %f", sGradeOption, [student getWorstGrade:sGradeOption]);
    NSLog(@" Avarage of CPS: %@: %f", sGradeOption, [student getAverageGrade:sGradeOption]);

    // NSLog(@" %@ ", [student description]);

  }
  return 0;
}