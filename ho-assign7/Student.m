//
//  Student.m
//  ho-assign7
//
//  Created by Henrique de Oliveira Carvalho on 2014-10-21.
//  Copyright (c) 2014 beta. All rights reserved.
//
//
// Problem Statement:
// • Create a class Student that has three NSMutableArrays of grades.
// • The Student constructor must verify that both first and last names
//   are between 2 and 20 characters long.
// • Create getters for both first and last names.
// • Create a class called StringFileUtils that is used to read from a file,
//   extract data of student.
// • Each student names and grades are available in their own files. Valid
//   grades are only case insensitive course codes IMG, CPS, and ENG and
//   grades from 0 to 100.
// • Any corrupted data should be ignored.
// • Read the files and fill the appropriate NSMutableArrays of grades for
//   each student.
// • The class Student should have three methods : getBestGrade,
//   getWorstGrade, and getAverageGrade that accepts a course code argument.
// • The class Student should have a method displayGrades of a specific grade
// • The class Student should have a method description that displays all
//   information about a Student.
// • Your driver main.m should contain a loop with a menu options asking the
//   user what actions he/she would like to do
//
// Inputs:   none
// Outputs:  Information about a student
//
// ******************************************************************************

#import "Student.h"

@implementation Student
{
  NSString * firstName;
  NSString * lastName;
}

- (instancetype) initMyArray
{
  self = [super init];
  if (self)
  {
    _cps = [[NSMutableArray alloc] init];
    _eng = [[NSMutableArray alloc] init];
    _img = [[NSMutableArray alloc] init];
  }
  return self;
}

- (NSString *) firstName
{
  return firstName;
}

- (NSString *) lastName
{
  return lastName;
}

// General construtor
- (void) setKey:(NSString *)key andValue:(NSString *)value
{
  // test if key is "first" or "last", i.e. no number in value
  if ([key compare:@"first"] == NSOrderedSame )
  {
    // Set first name
    firstName = @"";

    // verify that the first name is between 2 and 20 characters long
    if (value.length >= 2 && value.length <= 20)
    {
      firstName = value;
    }
  }
  else if (([key compare:@"last"] == NSOrderedSame))
  {
    // Set last name
    lastName = @"";

    // verify that the last name is between 2 and 20 characters long
    if (value.length >= 2 && value.length <= 20)
    {
      lastName = value;
    }
  }
  else
  {
    double grade = [value doubleValue];

    if (grade >= 0 && grade <= 100)
    {
      NSNumber * mark = [NSNumber numberWithDouble:grade];

      if ([key compare:@"cps"] == NSOrderedSame)
      {
        // if cps - place into cps array
        [_cps addObject:mark];
      }
      else if ([key compare:@"eng"] == NSOrderedSame)
      {
        // if eng - place into eng array
        [_eng addObject:mark];
      }
      else if ([key compare:@"img"] == NSOrderedSame)
      {
        // if img - place into img array
        [_img addObject:mark];
      }
    }
  }

}

// design patterns I created a expert method
- (BOOL) hasCourse:(NSString *)course
{
  course = [course lowercaseString];

  if ([course compare:@"cps"] == NSOrderedSame || [course compare:@"eng"] == NSOrderedSame || [course compare:@"img"] == NSOrderedSame)
  {
    return YES;
  }
  else
  {
    return NO;
  }
}

// design patterns I created a expert method
- (NSMutableArray *) getArrayCourse:(NSString *)course
{
  course = [course lowercaseString];

  if ([course compare:@"cps"] == NSOrderedSame)
  {
    return _cps;
  }
  else if ([course compare:@"eng"] == NSOrderedSame)
  {
    return _eng;
  }
  else if ([course compare:@"img"] == NSOrderedSame)
  {
    return _img;
  }
  else
  {
    return 0;
  }

}

- (void) showCourseGrades:(NSString *)course
{
  course = [course lowercaseString];

  NSMutableArray * whatCourse = [[NSMutableArray alloc] init];

  if ( [self hasCourse:course] == YES )
  {

    whatCourse = [self getArrayCourse:course];

    NSLog(@"%@ grades are: ", course);
    for (int pos = 0; pos < [whatCourse count]; pos++)
    {
      NSLog(@"%@", whatCourse[pos]);
    }

  }
  else
  {
    NSLog(@"There isn't this course!");
  }

}

- (double) getBestGrade:(NSString *)course
{
  double grade;
  double bestGrade = 0;

  NSMutableArray * whatCourse = [[NSMutableArray alloc] init];

  course = [course lowercaseString];

  if ( [self hasCourse:course] == YES )
  {

    whatCourse = [self getArrayCourse:course];

    for (int pos = 0; pos < [whatCourse count]; pos++)
    {
      grade = [whatCourse[pos] doubleValue];

      if (bestGrade < grade)
      {
        bestGrade = grade;
      }
    }

  }

  return bestGrade;
}


- (double) getWorstGrade:(NSString *)course
{
  double grade;
  double worstGrade = 1000;

  NSMutableArray * whatCourse = [[NSMutableArray alloc] init];

  course = [course lowercaseString];

  if ( [self hasCourse:course] == YES )
  {

    whatCourse = [self getArrayCourse:course];

    for (int pos = 0; pos < [whatCourse count]; pos++)
    {
      grade = [whatCourse[pos] doubleValue];

      if (worstGrade > grade)
      {
        worstGrade = grade;
      }
    }

  }
  else
  {
    return 0;
  }

  return worstGrade;
}

- (double) getAverageGrade:(NSString *)course
{
  double avg = 0, sum;
  NSMutableArray * whatCourse = [[NSMutableArray alloc] init];

  course = [course lowercaseString];

  if ( [self hasCourse:course] == YES )
  {

    whatCourse = [self getArrayCourse:course];

    if ([course compare:course] == NSOrderedSame)
    {
      for (int pos = 0; pos < [whatCourse count]; pos++)
      {
        sum += [whatCourse[pos] doubleValue];
      }

      avg = sum / [whatCourse count];

    }
  }

  return avg;
}

// method "description" to display values of instance variables
- (NSString *) description
{
  NSString * str = @"";

  str = [str stringByAppendingString:@"First Name: "];
  str = [str stringByAppendingFormat:@"%@", firstName];

  str = [str stringByAppendingString:@"\tLast Name: "];
  str = [str stringByAppendingFormat:@"%@\n", lastName];

  str = [str stringByAppendingString:@"\nCPS grades:\n"];
  for (int pos = 0; pos < [_cps count]; pos++)
  {
    str = [str stringByAppendingFormat:@"%@\n", _cps[pos]];
  }

  str = [str stringByAppendingString:@"\nENG grades:\n"];
  for (int pos = 0; pos < [_eng count]; pos++)
  {
    str = [str stringByAppendingFormat:@"%@\n", _eng[pos]];
  }

  str = [str stringByAppendingString:@"\nIMG grades:\n"];
  for (int pos = 0; pos < [_img count]; pos++)
  {
    str = [str stringByAppendingFormat:@"%@\n", _img[pos]];
  }

  return str;
}
@end
