//
//  StringFileUtils.h
//  ho-assign7
//
//  Created by Henrique de Oliveira Carvalho on 2014-10-21.
//  Copyright (c) 2014 beta. All rights reserved.
//
//
// Problem Statement:
// • Create a class Student that has three NSMutableArrays of grades.
// • The Student constructor must verify that both first and last names
//   are between 2 and 20 characters long.
// • Create getters for both first and last names.
// • Create a class called StringFileUtils that is used to read from a file,
//   extract data of student.
// • Each student names and grades are available in their own files. Valid
//   grades are only case insensitive course codes IMG, CPS, and ENG and
//   grades from 0 to 100.
// • Any corrupted data should be ignored.
// • Read the files and fill the appropriate NSMutableArrays of grades for
//   each student.
// • The class Student should have three methods : getBestGrade,
//   getWorstGrade, and getAverageGrade that accepts a course code argument.
// • The class Student should have a method displayGrades of a specific grade
// • The class Student should have a method description that displays all
//   information about a Student.
// • Your driver main.m should contain a loop with a menu options asking the
//   user what actions he/she would like to do
//
// Inputs:   none
// Outputs:  Information about a student
//
// ******************************************************************************

#import <Foundation/Foundation.h>

@interface StringFileUtils : NSObject

- (NSString *) getConsoleLine;
- (NSArray *) getNSArrayOfLinesFromFolder:(NSString *)folderName andFilename:(NSString *)fileName;
- (NSString *) getCompleteFileNameFromFolder:(NSString *)folderName andFileName:(NSString *)fileName;
- (BOOL) writeArrayOfStringsToXMLFile:(NSArray *)data toFolder:(NSString *)folderName toFile:(NSString *)fileName;
- (NSArray *) readArrayOfStringsFromXMLFileFromFolder:(NSString *)folderName fromFile:(NSString *)fileName;

@end