//
//  Student.h
//  ho-assign7
//
// Problem Statement:
// • Create a class Student that has three NSMutableArrays of grades.
// • The Student constructor must verify that both first and last names
//   are between 2 and 20 characters long.
// • Create getters for both first and last names.
// • Create a class called StringFileUtils that is used to read from a file,
//   extract data of student.
// • Each student names and grades are available in their own files. Valid
//   grades are only case insensitive course codes IMG, CPS, and ENG and
//   grades from 0 to 100.
// • Any corrupted data should be ignored.
// • Read the files and fill the appropriate NSMutableArrays of grades for
//   each student.
// • The class Student should have three methods : getBestGrade,
//   getWorstGrade, and getAverageGrade that accepts a course code argument.
// • The class Student should have a method displayGrades of a specific grade
// • The class Student should have a method description that displays all
//   information about a Student.
// • Your driver main.m should contain a loop with a menu options asking the
//   user what actions he/she would like to do
//
// Inputs:   none
// Outputs:  Information about a student
//
// ******************************************************************************

#import <Foundation/Foundation.h>

@interface Student : NSObject

@property NSMutableArray * cps, * eng, * img;

- (instancetype) initMyArray;

- (void) setKey:(NSString *)key andValue:(NSString *)value;
- (void) showCourseGrades:(NSString *)course;

// getters for both first and last names.
- (NSString *) firstName;
- (NSString *) lastName;

- (double) getBestGrade:(NSString *)course;
- (double) getWorstGrade:(NSString *)course;
- (double) getAverageGrade:(NSString *)course;

// design patterns I created expert methods
- (BOOL) hasCourse:(NSString *)course;
- (NSMutableArray *) getArrayCourse:(NSString *)course;

- (NSString *) description;
@end
